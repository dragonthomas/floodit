require 'console_splash'
require 'io/console'

def start
  splash = ConsoleSplash.new(15,40)
  splash.write_top_pattern("*")
  splash.write_bottom_pattern("*")
  splash.write_left_pattern("*")
  splash.write_right_pattern("*")
  
  splash.write_center(4, "Welcome to Flood-It", {:fg=>:yellow})
  splash.write_center(7, "version 1.0", {:fg=>:green})
  splash.write_center(8, "created by Thomas Tsoi", {:fg=>:green})
  splash.write_center(10, "<Press Enter to start>", {:fg=>:blue})
  splash.splash
end

def key_check
  input = gets
  if input == "\n"
    menu
  end
end

def menu
  puts 'Main menu'
  puts 's = Start game'
  puts 'c = Change size'
  puts 'q = quit'
  #counter
  puts 'Please enter your choice'
  read_char
end

def read_char
  STDIN.echo = false
  STDIN.raw!
  ch = STDIN.getc.chr
  if ch == "s"
    puts 's'
    get_board
  end
  if ch == "c"
    w = 14
    h = 9
    puts 'Width (currently #{w})? '
    w = gets.to_i
    puts 'Height (currently #{h})? '
    h = gets.to_i
    menu
  end
  if ch == "q"
    exit
  end
end

def get_board(width, height)
  # TODO: Implement this method
  #
  # This method should return a two-dimensional array.
  # Each element of the array should be one of the
  # following values (These are "symbols", you can use
  # them like constant values):
  # :red
  # :blue
  # :green
  # :yellow
  # :cyan
  # :magenta
  #
  # It is important that this method is used because
  # this will be used for checking the functionality
  # of your implementation.
end

start
key_check